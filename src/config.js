const z = require("zod");
const { readFile } = require("@theoparis/config");
const configSchema = z.object({
    port: z
        .number()
        .transform(
            z.number().optional(),
            (arg) => arg || process.env.PORT || 5000
        ),
});

export const config = readFile("config.yaml", {
    type: "yaml",
    schema: configSchema,
}).validate(true);
